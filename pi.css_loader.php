<?php if ( !defined ('BASEPATH')) exit ('No direct script access allowed');

// author: Dennis Bond, Michigan State University (bonddenn@msu.edu)
// February 4, 2010
// edited 10/19/2011 to reorder the stylesheets per the Web Designer's request
// Renamed June 23, 2014 for responsive reference design

$plugin_info = array (
	'pi_name'		=> 'CSS Loader',
	'pi_version'		=> '2.0',
	'pi_author' 		=> 'Dennis Bond',
	'pi_description' 	=> 'Provides the link tags needed to include CSS files in the header'
);

class Css_loader
{
	// need this because constructors cannot return data
	var $return_data = "";

	function Css_loader()
	{
		// get an instance of the ExpressionEngine object
		$this->EE =& get_instance();
		
		// fetch data
		$site_template_group = $this->EE->TMPL->fetch_param('site_template_group') ? $this->EE->TMPL->fetch_param('site_template_group') : "pages";
		$href_list = $this->EE->TMPL->fetch_param('additional');
		$breadcrumbs = $this->EE->TMPL->fetch_param('breadcrumbs');
		$anr_bar = $this->EE->TMPL->fetch_param('anr_bar');
		
		// break the plugins list into an array
		$hrefs = explode(",", $href_list);
		
		// generate the css in order
		$css_code = "";
       
		// additional stylesheets
		foreach ($hrefs as $href)
			if ( trim($href) )
				$css_code .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://expeng.anr.msu.edu/css/".$href.".css\" />\n";
       
		// site styles
		$css_code .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"{stylesheet=".$site_template_group."/styles}\" />\n";
		
		$this->return_data = $css_code;
	}
}
?>